var CARD_TEXT_X = 10;
var CARD_TEXT_Y = 4;
var CARD_TEXT_WIDTH = 200;
var CARD_LINE_HEIGHT = -5;

var CARD_BUTTON_WIDTH = 200;
var CARD_BUTTON_HEIGHT = 20;
var CARD_BUTTON_SPACING = 10;
var CARD_BUTTON_COLOR = "#6985fb"

Card = function(gameParam, cardData) {
	this.game = gameParam,
	this.titleString = cardData.titleText;
	this.bodyString = cardData.bodyString;
	this.titleImageUrl = cardData.titleImageUrl;

	this.optionData = cardData.options;
	this.optionButtons = [];

	this.clicked = new Phaser.Signal();
	this.hasBeenClicked = false;
}

Card.prototype = {
	create: function () {
		this.titleText = this.game.add.text(CARD_TEXT_X, CARD_TEXT_Y,
			this.titleString.toUpperCase(), PHONE_FONT);
		this.titleText.wordWrap = true;
		this.titleText.wordWrapWidth = CARD_TEXT_WIDTH;
		this.titleText.lineSpacing = CARD_LINE_HEIGHT;

		this.bodyText = this.game.add.text(CARD_TEXT_X, 
			this.titleText.y + this.titleText.height + CARD_TEXT_Y,
			this.bodyString, PHONE_FONT);
		this.bodyText.wordWrap = true;
		this.bodyText.wordWrapWidth = CARD_TEXT_WIDTH;
		this.bodyText.lineSpacing = CARD_LINE_HEIGHT;

		//add to group
		this.group = this.game.add.group();
		this.group.add(this.titleText);
		this.group.add(this.bodyText);

    	var numOptions = this.optionData.length;
		for(var i = 0; i < numOptions; i++) {
			var newButton = this.createOptionButton(this.optionData[i]);
			newButton.x = CARD_TEXT_X;
			newButton.y = 
				this.bodyText.y + this.bodyText.height + CARD_BUTTON_SPACING +
				i * (CARD_BUTTON_HEIGHT + CARD_BUTTON_SPACING);
			this.group.add(newButton);
		}

		this.group.visible = false;
    },

    createOptionButton: function(optionData) {
    	// create a new bitmap data object
    	var bmd = game.add.bitmapData(CARD_BUTTON_WIDTH, CARD_BUTTON_HEIGHT);

	    // draw to the canvas context like normal
	    bmd.ctx.beginPath();
	    bmd.ctx.rect(0, 0, CARD_BUTTON_WIDTH, CARD_BUTTON_HEIGHT);
	    bmd.ctx.fillStyle = CARD_BUTTON_COLOR;
	    bmd.ctx.fill();

	    // use the bitmap data as the texture for the sprite
	    var sprite = game.add.sprite(0, 0, bmd);

	    var label = this.game.add.text(0, 0, optionData.text, PHONE_FONT);
	    sprite.addChild(label);
	    
	    label.x = (sprite.width - label.width) / 2.0;
	    label.y = (sprite.height - label.height) / 2.0;

	    var effectName = optionData.effectName;
	    var effectPoints = 10;

	    sprite.inputEnabled = true;
	    sprite.events.onInputUp.add(this.onButtonClick, this, 0, 
	    	effectName, effectPoints);

	    return sprite;
    },

    onButtonClick: function(target, arg2, arg3, effectName, effectPoints) {
    	this.hasBeenClicked = true;
    	this.clicked.dispatch(effectName, effectPoints);
    },

    update: function () {

        //  Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!

    },

    hide: function() {
    	this.group.visible = false;
    },

    set inputEnabled(value) {
    	var numOptions = this.optionButtons.length;
		for(var i = 0; i < numOptions; i++) {
			this.optionButtons[i].inputEnabled = value;
		}
    },
}