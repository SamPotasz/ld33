RoshHashana = function (game) {};

var AVINU_DELAY = Phaser.Timer.SECOND * 5;
var LOOP_DELAY = Phaser.Timer.SECOND;

var TEXT_SNIPPETS = [
	"What will you do better this year?",
	"Are you free?",
	"When do you conform?",
	"What have you learned so far?",
	"What could we sacrifice to change the world?",
	"Judges you shall put in all your gates.",
	"What unfinished business is tearing your focus away from the present?",
	"What shadow of fear or anger is keeping us from connecting?",
	"Why do we run after ambition?",
	"Why do we need fame and fortune?",
	"Why do we need sexual conquest?",
	"Our parent, our sovereign, be gracious to us, and answer us even though we don't deserve it.",
	"We have no good deeds to invoke in our defense.",
	"Our origin is dust and our end is dust.",
	"God is close to the broken hearted.",
];

var SNIPPET_WIDTH = 300;
var SNIPPET_TWEEN_DURATION = Phaser.Timer.SECOND * 10;

RoshHashana.prototype = {

	preload: function() {
		// this.widget = SC.Widget(document.getElementById('soundcloud_widget'));
		// this.waves = this.game.add.audio('waves');
	},

	create: function () {
		this.waves.loopFull(1.0);

		this.game.time.events.loop(LOOP_DELAY, this.displaySnippet, this);

		this.game.time.events.add(AVINU_DELAY, this.playWidget, this);
	},

	displaySnippet: function() {
		var index = this.rnd.between(0, TEXT_SNIPPETS.length - 1);

		var startX = this.rnd.between(10, this.game.width - SNIPPET_WIDTH);
		
		var snippet = this.game.add.text(
			startX, this.game.height + 20,
			TEXT_SNIPPETS[index]);
		snippet.wordWrapWidth = SNIPPET_WIDTH;
		snippet.lineSpacing = -5;

		var endX = this.rnd.between(10, this.game.width - SNIPPET_WIDTH);
		var endY = -snippet.height - 20;

		var tween = this.game.add.tween(snippet)
        tween.to({x: endX, y:endY}, SNIPPET_TWEEN_DURATION, null);
        tween.start();
	},

	playWidget: function() {
		this.widget.play();
	},

	update: function () {

		//	Do some nice funky main menu effect here

	},
};
