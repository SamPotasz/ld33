var PHONE_FONT = {font: '14px Maven Pro', fill: 'white', stroke: 'white'};
var GRAY_BG_COLOR = "#343d3f";

var CAFE_VOLUME = 0.1;

var CARD_X = 290;
var CARD_Y = 135;

// var CARD_START_DURATION = 5;
// var CARD_SPEED_UP = 0.2;
// var CARD_MIN_DURATION = 0;

var STATS_VISIBLE = true;

var NUM_SLOW_CARDS = 13;
var NUM_QUICK_CARDS = 60;
var QUICK_CARD_DURATION = 90;
var QUICK_CARD_MIN_DURATION = 1;
var QUICK_CARD_DECREASE = 3;


var CAM_DRAW_WIDTH = 227;
var CAM_DRAW_HEIGHT = 399;
var CAM_DRAW_X = CARD_X;
var CAM_DRAW_Y = CARD_Y;

var CAM_WIDTH = 640;
var CAM_HEIGHT = 480;


// var FINAL_CARD = 
//     {
//         "titleText": "S has updated their status",
//         "titleImageUrl": "",
//         "bodyString": "Friends and family,\n\nWe are sad to announce the passing of our dear sweet child M. We know M was beloved by many and will be deeply missed. Please join us in honoring M's life this Sunday at 3pm with services at 120 E. Cullen St.\n\nReception to follow.",
//         "options": []
//     };
var FIRST_CARD = {
    "titleText": "",
    "titleImageUrl": "",
    "bodyString": "We Have No Good Deeds",
    "options": [
        {
            "text": "Play",
            "effectName": "",
            "effectPoints": 0,
        }
    ]
};

var FINAL_CARD_DURATION = 8;

var LABEL_X = 600;
var LABEL_Y = 30;
var LABEL_HEIGHT = 30;
var LABEL_FONT = {font: '20px Maven Pro', fill: 'white', stroke: 'white'};
var DISPLAY_RIGHT_X = 725;

var START_TIME = 100;
var TICK_DURATION = Phaser.Timer.SECOND / 2.0;

PhoneScreen = function (game) {};

PhoneScreen.prototype = {

    preload: function() {
        this.cards = [];
        this.currentCard = null;
        this.currentCardIndex = 0;

        var numCards = allCardData.length;
        for(var i = 0; i < numCards; i++) {
            var card = new Card(this.game, allCardData[i]);
            card.clicked.add(this.onCardClicked, this);
            this.cards.push(card);
        }

        this.numClicked = 0;

        this.pickingQuickly = false;
        this.numPickedQuickly = 0;
        this.ticksSinceLastPicked = 0;
        this.quickTicks = QUICK_CARD_DURATION;

        this.cafeSound = this.game.add.audio('cafe');
        this.tekiah = this.game.add.audio('tekiah');

        this.displays = {
            "wealth": null,
            "fame": null,
            "sex": null,
            "time": null,
        };

        // this.cardTimer = new Phaser.Timer(this.game);
        // this.currCardDuration = TICK_DURATION;
        // console.log("currCardDuration" + this.currCardDuration);

        this.cardsLeftDisplay = null;
    },

    create: function () {
        console.log("Creating PhoneScreen");
        this.background = this.add.sprite(0, 0, 'titlepage');
        this.cafeSound.loopFull(CAFE_VOLUME);

        this.createCamera();

        var numCards = this.cards.length;
        console.log("Creating " + numCards + " cards.");
        for(var i = 0; i < numCards; i++) {
            this.cards[i].create();
        }

        // set up displays
        var displayNumber = 0;
        for(var name in this.displays){
            var displayY = LABEL_Y + displayNumber * LABEL_HEIGHT;
            

            this.displays[name] = this.game.add.text(
                0, displayY, "0", LABEL_FONT);
            this.displays[name].x = DISPLAY_RIGHT_X - this.displays[name].width;
            this.displays[name].value = 0;
            this.displays[name].visible = false;

            var label = this.game.add.text(
                LABEL_X, displayY,
                name.toUpperCase() + ":", LABEL_FONT);
            label.visible = false;

            this.displays[name].label = label;

            displayNumber++;
        }

        //start counting down
        this.displays.time.setText(START_TIME);
        this.displays.time.value = START_TIME;

        this.cardsLeft = NUM_SLOW_CARDS + NUM_QUICK_CARDS;
        this.cardsLeftDisplay = this.game.add.text(
            LABEL_X, LABEL_Y, "Cards Left: " + this.cardsLeft,
            LABEL_FONT);
        this.cardsLeftDisplay.visible = false;

        //show the first card
        this.showFirstCard();
        // this.pickNewRandomCard();
    },

    showFirstCard: function() {
        var firstCard = new Card(this.game, FIRST_CARD);
        firstCard.clicked.add(this.onCardClicked, this);
            
        firstCard.create();
        this.currentCard = firstCard;
        var displayGroup = firstCard.group;
        displayGroup.visible = true;
        displayGroup.x = CARD_X;
        displayGroup.y = CARD_Y;
    },

    createCamera: function() {
        console.log("creating camera");
        game.stage.backgroundColor = 0x4488cc;

        this.offsetX = CAM_WIDTH/2 - CAM_DRAW_WIDTH/2;
        this.offsetY = CAM_HEIGHT/2 - CAM_DRAW_HEIGHT/2;


        this.shutterSound = game.add.sound('shutter', 0.8);
        this.buttonSound = game.add.sound('click');
        this.readySound = game.add.sound('ready');
        this.beepSound = game.add.sound('beep', 0.3);

        // Setup camera
        this.camBitmap = game.add.bitmapData(CAM_WIDTH, CAM_HEIGHT, 'gameCofdklfdsantainer');
        this.cam = new Phaser.Plugin.Webcam(game, this);

        this.webcamAvailable = !(navigator.getUserMedia === undefined);
        if (!this.webcamAvailable) {
            // document.getElementById('instructions').style.display = "none";
            // document.getElementById('unsupported').style.display = "block";
            // document.getElementById('cam').style.display = "none";
        } else {
            console.log("camera is available");
            this.cam.start(this.camBitmap.width, this.camBitmap.height, this.camBitmap.context);
            this.cam.onConnect.add(this.cameraConnected, this);
            this.cam.onError.add(this.cameraError, this);
            game.add.plugin(this.cam);
        }

        // Setup working canvas
        this.pixelBitmap = game.add.bitmapData(CAM_DRAW_WIDTH, CAM_DRAW_HEIGHT);

        // Setup final display surface
        this.surface = game.add.sprite(CAM_DRAW_X, CAM_DRAW_Y, this.pixelBitmap);
        this.surface.alpha = 0.5;

        this.pixelSize = 5;
        // Message to turn on the camera
        // this.turnOnCamera = game.add.image(0, 0, 'sprites', 'turn-on-camera.png');
        // this.turnOnCamera.scale.set(2);

        // Add UI
        // this.ui = game.add.group();
        // game.add.image(0, game.height/2 - 21, 'sprites', 'button-panel.png', this.ui);
    },

    onTick: function() {
        // console.log("tick. timeLeft: " + this.displays.time.value);
        this.displays.time.value--;
        this.displays.time.setText(this.displays.time.value);
    },

    displayCard: function(cardIndex, inputEnabled) {
        if(this.currentCard){
            // this.game.time.events.stop();
            // this.game.time.events.removeAll();
            // this.currCardDuration -= this.CARD_SPEED_UP;

            this.currentCard.hide();
        }

        this.currentCard = this.cards[cardIndex];
        var displayGroup = this.currentCard.group;
        console.log("Displaying " + this.currentCard.titleText.text);
        displayGroup.visible = true;
        displayGroup.x = CARD_X;
        displayGroup.y = CARD_Y;
        this.currentCard.inputEnabled = inputEnabled;

        // console.log("currCardDuration: " + this.currCardDuration);
        // this.game.time.events.add(this.currCardDuration, this.onCardTimerTick, this);
        // this.cardTimer.start();
        // console.log("card timer: ");
        // console.log(this.cardTimer);

        this.cardsLeft--;
        this.cardsLeftDisplay.setText("Cards Left: " + this.cardsLeft);
        this.cardsLeftDisplay.visible = false;

        this.currentCardIndex = cardIndex;
    },

    onCardTimerTick: function() {
        console.log("Timer clicked!");
        this.pickNewRandomCard();
    },

    onCardClicked: function(effectName, effectPoints) {
        if(this.numClicked == 0) {
            //start timer, show displays
            for(var name in this.displays) {
                this.displays[name].visible = true;
                this.displays[name].label.visible = true;
            }

            this.timer = this.game.time.events;
            this.timer.loop(TICK_DURATION, this.onTick, this);
        }

        this.numClicked++;

        if(effectName.length > 0) {
            this.addEffects(effectName, effectPoints);
        }

        if(this.numClicked >= this.cards.length){
            console.log("Done!");
        }
        else if(this.numClicked < NUM_SLOW_CARDS){
            this.pickNewRandomCard();
        }
        else{
            this.pickCardsQuickly();
        }
    },

    addEffects: function(effectName, effectPoints) {
        var display = this.displays[effectName];
        if(typeof display !== "undefined") {
            display.value += effectPoints;
            display.setText(display.value);
        }

        var popup = this.game.add.text(
            CARD_X - 140, CARD_Y,
            "+" + effectPoints + " " + effectName, LABEL_FONT);
        var tween = this.game.add.tween(popup)
        tween.to({ y: popup.y - 50 }, Phaser.Timer.SECOND * 1.0, null);
        tween.onComplete.add(function(){popup.destroy();})
        tween.start();
    },

    pickNewRandomCard: function() {
        var randomIndex = this.rnd.between(0, this.cards.length - 1);
        while(this.cards[randomIndex].hasBeenClicked) {
            randomIndex = this.rnd.between(0, this.cards.length - 1);
        }

        this.displayCard(randomIndex, true);
    },

    pickCardsQuickly: function() {
        console.log("picking cards quickly now!");
        this.pickingQuickly = true;
        console.log("isRunning? " + this.timer.isRunning);
        this.timer.stop();
        console.log("isRunning? " + this.timer.isRunning);
        this.pickQuickCard();
    },

    update: function() {
        if(this.pickingQuickly) {
            this.ticksSinceLastPicked++;
            if(this.ticksSinceLastPicked >= this.quickTicks) {
                this.pickQuickCard();
            }
        }

        if(this.webcamAvailable){
            this.pixelate();
        }
    },

    pickQuickCard: function() {
        this.numPickedQuickly++;
        this.currentCard.hide();

        this.ticksSinceLastPicked = 0;
        this.quickTicks -= QUICK_CARD_DECREASE;
        this.quickTicks = Math.max(QUICK_CARD_MIN_DURATION, this.quickTicks);
        
        if(this.numPickedQuickly >= NUM_QUICK_CARDS) {
        // if(this.quickTicks <= QUICK_CARD_MIN_DURATION){
            this.displayFinalCard();
        }
        else{
            this.displays.time.value = Math.max(0, this.displays.time.value - 3);
            this.displays.time.setText(this.displays.time.value);

            var randomIndex = this.rnd.between(0, this.cards.length - 1);
            while(randomIndex == this.currentCardIndex) {
                randomIndex = this.rnd.between(0, this.cards.length - 1);
            }
            this.displayCard(randomIndex, false);
        }
    },

    displayFinalCard: function() {
        this.pickingQuickly = false;

        for(var name in this.displays) {
            this.displays[name].visible = false;
            this.displays[name].label.visible = false;
        }
        this.displays.time.value = 0;
        this.displays.time.setText(this.displays.time.value);
        this.displays.time.visible = true;
        this.displays.time.label.visible = true;

        var bodyString = "WEALTH: " + this.displays.wealth.value + "\n" +
            "SEX: " + this.displays.sex.value + "\n" +
            "FAME: " + this.displays.fame.value + "\n";

        var FINAL_CARD = {
            "titleText": "-- Final Tally --",
            "titleImageUrl": "",
            "bodyString": bodyString,
            "options": []
        };

        var finalCard = new Card(this.game, FINAL_CARD);
        finalCard.create();

        var displayGroup = finalCard.group;
        displayGroup.visible = true;
        displayGroup.x = CARD_X;
        displayGroup.y = CARD_Y;
        this.currentCard = finalCard;

        this.scoreText = this.game.add.text(
            CARD_X, CARD_Y + displayGroup.height + 20,
            "TOTAL SCORE: 0", LABEL_FONT);
        this.scoreText.visible = false;
        this.game.time.events.add(Phaser.Timer.SECOND * 4,
            this.displayFinalScore, this);

        this.cafeSound.stop();
        this.playTekiah();

        this.game.time.events.add(Phaser.Timer.SECOND * FINAL_CARD_DURATION, this.afterFinalCard, this);
        this.game.time.events.start();
    },

    displayFinalScore: function() {
        console.log("displaying final score");
        this.scoreText.visible = true;
    },

    afterFinalCard: function() {
        this.game.time.events.add(Phaser.Timer.SECOND, this.fadeOut, this);
        // this.game.time.events.add(Phaser.Timer.SECOND * 10, this.goToRosh, this);
    },

    //http://www.html5gamedevs.com/topic/2016-rectangle-fade/
    fadeOut: function() {
        var FADE_DURATION = Phaser.Timer.SECOND * 2.5;

        console.log("Fading out");
        var spr_bg = this.game.add.graphics(0, 0);
        this.background.addChild(spr_bg);
        spr_bg.beginFill("#000000", 1);
        spr_bg.drawRect(0, 0, this.game.width, this.game.height);
        spr_bg.alpha = 0;
        spr_bg.endFill();

        s = this.game.add.tween(spr_bg)
        s.to({ alpha: 1.0 }, FADE_DURATION, null);
        
        var faceTween = this.game.add.tween(this.surface);
        faceTween.to({alpha: 1.0}, FADE_DURATION, null);

        for(var name in this.displays){
            var fadeDisplay = this.game.add.tween(this.displays[name]);
            fadeDisplay.to({alpha: 0.0}, FADE_DURATION, null);
            fadeDisplay.start();

            var fadeLabel = this.game.add.tween(this.displays[name].label);
            fadeLabel.to({alpha: 0.0}, FADE_DURATION, null);
            fadeLabel.start();
        }    

        var scoreFade = this.game.add.tween(this.scoreText);
        scoreFade.to({alpha: 0.0}, FADE_DURATION, null);
        scoreFade.start();

        var tallyFade = this.game.add.tween(this.currentCard.group);
        tallyFade.to({alpha: 0.0}, FADE_DURATION, null);
        tallyFade.start();

        s.start();
        faceTween.start();

        var restartTime = FADE_DURATION + Phaser.Timer.SECOND * 5;
        this.timer.add(restartTime, this.showRestart, this);
    },

    showRestart: function() {
        var bmd = game.add.bitmapData(CARD_BUTTON_WIDTH, CARD_BUTTON_HEIGHT);

        // draw to the canvas context like normal
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, CARD_BUTTON_WIDTH, CARD_BUTTON_HEIGHT);
        bmd.ctx.fillStyle = CARD_BUTTON_COLOR;
        bmd.ctx.fill();

        // use the bitmap data as the texture for the sprite
        this.restartButton = game.add.button(game.width / 2 - bmd.width / 2, 
            game.height * 4/5 + 30);

        var label = this.game.add.text(0, 0, "Restart", PHONE_FONT);
        this.restartButton.addChild(label);

        label.x = (this.restartButton.width - label.width) / 2.0;

        this.restartButton.inputEnabled = true;
        this.restartButton.events.onInputUp.add(this.onRestartClick, this);


        this.endTitle = this.game.add.text(0, 40,
            "We have no good deeds.", LABEL_FONT);
        this.endTitle.x = (game.width - this.endTitle.width) / 2.0;
    },

    onRestartClick: function() {
        this.numClicked = 0;

        this.currentCard.alpha = 1.0;
        for(var name in this.displays) {
            var display = this.displays[name];
            display.alpha = 1.0;
            display.visible = true;
            display.label.alpha = 1.0;
            display.label.visible = true;
            display.value = 0;
            display.setText(display.value);
        }
        this.displays.time.setText(START_TIME);
        this.displays.time.value = START_TIME;

        this.background.removeChildren();

        var numCards = 0;
        for(var i = 0; i < numCards; i++) {
            this.cards[i].hasBeenClicked = false;
        }

        this.restartButton.visible = false;
        this.endTitle.visible = false;

        this.showFirstCard();

        this.cafeSound.loopFull(CAFE_VOLUME);
    },

    playTekiah: function() {
        this.tekiah.play();
        // this.tekiah.onStop.add(this.goToRosh, this);
    },

    goToRosh: function() {
        this.state.start("RoshHashana");
    },

    cameraConnected: function() {
        console.log("camera is connected");
        // this.turnOnCamera.visible = false;
        // this.ui.visible = true;

        // this.readySound.play();
    },

    cameraError: function() {
        console.log("WEB CAM ERROR!");
        // document.getElementById('cam').style.display = "none";
        // document.getElementById('notconnected').style.display = "block";
        // document.getElementById('instructions').style.display = "none";
    },

    pixelate: function() {
        // console.log("pixelating");
        var pxContext = this.pixelBitmap.context;

        this.camBitmap.update();

        var pixel = Phaser.Color.createColor();

        for(var x = 0; x < CAM_DRAW_WIDTH; x += this.pixelSize) {
            for(var y = 0; y < CAM_DRAW_HEIGHT; y += this.pixelSize) {
                // Sample color at x+offsetX,y+offsetY in camBitmap
                this.camBitmap.getPixel(//x, y, pixel);
                    Math.floor(x + this.offsetX),
                    Math.floor(y + this.offsetY),
                    pixel);

                // Modify color
                // this.posterizeFilter(pixel, 16);
                // if (!this.color) this.grayscaleFilter(pixel);
                // var tint = this.tintChoices[this.tintValue];
                var tint = { r:1.0, g:1.5, b:2 };
                this.tintFilter(pixel, tint.r, tint.g, tint.b, 1.0/10.0);

                // Draw pixel at x,y in new bitmap
                pxContext.fillStyle = "rgb(" + pixel.r + "," + pixel.g + "," + pixel.b + ")"
                pxContext.fillRect(x, y, this.pixelSize, this.pixelSize);
            }
        }
        
        this.camBitmap.dirty = true;
        this.pixelBitmap.dirty = true;
    },

    tintFilter: function(pixel, r, g, b, magnitude) {
        pixel.r = Math.floor(pixel.r * r * magnitude);
        pixel.g = Math.floor(pixel.g * g * magnitude);
        pixel.b = Math.floor(pixel.b * b * magnitude);
    },
};
